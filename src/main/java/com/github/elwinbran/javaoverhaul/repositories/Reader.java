/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.repositories;

import com.github.elwinbran.javaoverhaul.numbers.PositiveNumber;
import com.github.elwinbran.javaoverhaul.containers.ContainerDecoration;
import com.github.elwinbran.javaoverhaul.exceptions.SafeReturn;
import com.github.elwinbran.javaoverhaul.queries.ReadFilter;
import java.util.Iterator;

/**
 * This represents a persistent storage for objects.
 * This storage provides Read-Only access to the persisted objects.
 * 
 * @author Elwin Slokker
 * @param <PersistableT> The type that is stored in this reeader.
 */
public interface Reader<PersistableT>
{
    /**
     * Reads or retrieves all files or records that qualify to be a {@code T}.
     * <br>Both lazy and eager implementation are allowed, so watch closely what
     * <br> implementation is used, because both can have surprising performance 
     * effects.
     * 
     * @param filter The extra argument that filters the entries/records read
     * from the storage.
     * @return An object that may carry the collection of read objects and/or 
     * <br> an exception message, when something went wrong.
     * <br>Refer to {@link SafeReturn the class itself} for the full 
     * specification.
     */
    public SafeReturn<ContainerDecoration<PersistableT>> read(
            ReadFilter<PersistableT> filter);
    
    
    
    /**
     * Very WIP. Very interesting.
     * Obviously made to cater the needs of getting too many results from a repo.
     * @param filter The extra argument that filters the entries/records read
     * from the storage.
     * @param maxResultPerPage The maximum amount of read results in every
     * 'collection' or 'page'.
     * @return 
     */
    public abstract Iterator<SafeReturn<ContainerDecoration<PersistableT>>> read(
            ReadFilter<PersistableT> filter, 
            PositiveNumber maxResultPerPage);
}
