/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.repositories;

import com.github.elwinbran.javaoverhaul.containers.ContainerDecoration;
import com.github.elwinbran.javaoverhaul.exceptions.ExceptionInformation;
import com.github.elwinbran.javaoverhaul.exceptions.SafeReturn;
import com.github.elwinbran.javaoverhaul.queries.DeletePredicate;
import com.github.elwinbran.javaoverhaul.queries.UpdateStatement;

/**
 * This represents a persistent storage for objects.
 * This storage provides CRUD access to the persisted objects.
 * [link]
 * 
 * @author Elwin Slokker
 * @param <PersistableT> The type that is stored in this repository.
 */
public interface Repository<PersistableT>
{
    /**
     * Creates persistent entries from domain objects and skips those that could
     * not be created.
     * <br><br>
     * 
     * @param newTs items/objects to be added.
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be created.
     * <br>Refer to {@link SafeReturn the class itself} for the full 
     * specification.
     */
    public abstract SafeReturn<ContainerDecoration<PersistableT>> create(
            ContainerDecoration<? extends PersistableT> newTs);
    
    /**
     * 
     * @return 
     */
    public abstract Reader readOnlyAccess();
    
    /**
     * Deletes all records/files from the repository.
     * <br>Use with extreme caution!
     * <br>-Will usually delete all files under a directory when the repository is a directory.
     * <br>-Will usually delete all entries from a DB table.
     * 
     * <br><br>Architectural note: DeleteAll is not part of regular CRUD.
     * <br>However, this was considered as an important method for the data 
     * <br>abstraction for cases where data corrupted or the current class 
     * <br>versions mismatch with that what is stored in the repository.
     * <br>This method may be powerless in some cases.
     * 
     * @return An object that carries a boolean indicating whether all 
     * <br>files/entries were deleted. When it returns {@code false} check the
     * <br>included exception message for details.
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     */
    public abstract ContainerDecoration<ExceptionInformation> clear();
    
    /**
     * Finds the older versions of the {@code newVersions} in the repository
     * <br>and updates them, skipping objects when unable to update.
     * <br><br>
     * 
     * @param newVersions The replacement objects/items.
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be updated.
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     */    
    public abstract SafeReturn<ContainerDecoration<PersistableT>> 
        update(ContainerDecoration<? extends PersistableT> newVersions);
    
    /**
     * Finds the older versions of the {@code newVersions} in the repository
     * <br>and updates them, skipping objects when unable to update.
     * <br><br>
     * 
     * @param statement The thing that is executed for every record/entry/object
     * in the storage (but this also includes a check, so some 
     * records/entries/objects are unaffected).
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be updated.
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     */    
    public abstract SafeReturn<ContainerDecoration<PersistableT>> update(
           UpdateStatement<? extends PersistableT> statement);
    
     /**
     * Deletes as much of {@code removedObjects} as possible.
     * 
     * @param removedObjects Items/objects to be deleted from the repository.
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be deleted (because of 
     * IO-issues).
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     */
    public abstract SafeReturn<ContainerDecoration<PersistableT>> delete(
            ContainerDecoration<? extends PersistableT> removedObjects);
    
    /**
     * Deletes as much of {@code removedObjects} as possible.
     * 
     * @param predicate Every entry/object in the storage is checked for this 
     * predicate and deleted if it is true.
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be deleted (no matter the
     * reason)
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     */
    public abstract SafeReturn<ContainerDecoration<PersistableT>> delete(
            DeletePredicate<? extends PersistableT> predicate);
}
