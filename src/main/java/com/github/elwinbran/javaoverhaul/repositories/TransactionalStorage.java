/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.repositories;

import com.github.elwinbran.javaoverhaul.containers.Container;
import com.github.elwinbran.javaoverhaul.exceptions.SafeReturn;
import com.github.elwinbran.javaoverhaul.uei.ExceptionInformation;

/**
 * 
 * @author Elwin Slokker
 */
public interface TransactionalStorage
{
    /**
     * Adds all {@code newTs} (or none when an exception occurs)to the 
     * repository.
     * <br><br>
     * 
     * @param newTs items/objects to be added.
     * @return If the returns {@code isEmpty == true} than the creation of 
     * {@code newTs} in the Repository has succeeded.
     * <br>Otherwise check the exceptions.
     */
    public abstract Container<ExceptionInformation> create(
            Container<PersistableT> newTs);
    
    /**
     * Reads or retrieves all files or records that qualify to be a {@code T}
     * and if not all can be read/retrieved, reads/retrieves none.
     * 
     * <br>Both lazy and eager implementation are allowed, so watch closely what
     * <br> implementation is used, because both can have surprising performance 
     * effects.
     * 
     * @return An object that may carry the collection of read objects and/or 
     * <br> an exception message, when something went wrong.
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     */
    public SafeReturn<Container<PersistableT>> read();
    
    /**
     * Updates all {@code newVersions} (or none when an exception occurs) in the 
     * repository.
     * <br><br>
     * 
     * @param statement The thing that is executed for every record/entry/object
     * in the storage (but this also includes a check, so some 
     * records/entries/objects are unaffected).
     * @return If the returns {@code isEmpty == true} than the creation of 
     * {@code newTs} in the Repository has succeed.
     * <br>Otherwise check the exceptions.
     */
    public Container<ExceptionInformation> update(
           UpdateStatement<? extends PersistableT, ? extends IndexT> statement);
    
    /**
     * Deletes all {@code removedObjects} (or none when an exception occurs) 
     * from the repository.
     * <br><br>
     * 
     * @param removedObjects Items/objects to be deleted from the repository.
     * @return If the returns {@code isEmpty == true} than the deletion of 
     * {@code removedObjects} in the repository has succeeded.
     * <br>Otherwise check the exceptions to see what went wrong.
     */
    public abstract Container<ExceptionInformation> delete(
            Container<? extends PersistableT> removedObjects);
    
    /**
     * Deletes all records/files from the repository.
     * <br>Use with extreme caution!
     * <br>-Will usually delete all files under a directory when the repository is a directory.
     * <br>-Will usually delete all entries from a DB table.
     * 
     * <br><br>Architectural note: DeleteAll is not part of regular CRUD.
     * <br>However, this was considered as an important method for the data 
     * <br>abstraction for cases where data corrupted or the current class 
     * <br>versions mismatch with that what is stored in the repository.
     * <br>This method may be powerless in some cases.
     * 
     * @return An object that carries a boolean indicating whether all 
     * <br>files/entries were deleted. When it returns {@code false} check the
     * <br>included exception message for details.
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     */
    public abstract Container<ExceptionInformation> clear();
    
}
