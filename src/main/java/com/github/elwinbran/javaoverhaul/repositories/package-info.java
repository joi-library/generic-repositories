/**
 * This package contains abstract definitions and generic implementations for 
 * loading data from outside.
 * TODO
 * 
 * 
    <br>all combinations:
    <br>SimpleReader&#60PersistableT, IndexT&#62
    <br>ReaderQueryable&#60PersistableT, IndexT&#62
    <br>WrappingSimpleReader&#60DomainT, PersistableT, IndexT&#62 //normal reader
    <br>WrappingReaderQueryable&#60DomainT, PersistableT, IndexT&#62
    
    <br>-always includes clear() and query methods.
    <br>FreeIndexRepository&#60PersistableT, IndexT&#62
    *   CFR and SR<IP>
    <br>GivenIndexRepository&#60DataOnlyPersistableT, PersistableT, IndexT&#62
    *   CIP and SR<CR<PP, IP, I>>
    <br>WrappingFreeIndexRepository&#60DomainT, PersistableT, IndexT&#62
    *   CRF and SR<WP>
    <br>WrappingGivenIndexRepository&#60DomainT, DataOnlyPersistableT, PersistableT, IndexT&#62
    *   CIP and SR<CR<D, PP, WP, I>>
    <br>
    <br>-no query variants:
    <br>
<style>
table, th, tr, td {
   border: 1px solid black;
}</style>
<table>
  <tr>
    <th></th>
    <th>virtual</th>
    <th>JSON</th>
    <th>BSON</th>
    <th>XML</th>
    <th>YAML</th>
    <th>Custom</th>
  </tr>
  <tr>
    <td>Local filesystem</td>
    <td>vq</td>
    <td>vq</td>
    <td>vq</td>
    <td>vq</td>
    <td>vq</td>
    <td>?</td>
  </tr>
  <tr>
    <td>MySQL</td>
    <td>q</td>
    <td>q</td>
    <td>vq</td>
    <td>vq</td>
    <td>vq</td>
    <td>?</td>
  </tr>
  <tr>
    <td>CouchDB</td>
    <td>q(with framework)</td>
    <td>q</td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    <td>?</td>
  </tr>
  <tr>
    <td>HTTP</td>
    <td>X</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>?</td>
  </tr>
  <tr>
    <td>HTTPS</td>
    <td>X</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>FTP</td>
    <td>X</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>SFTP</td>
    <td>X</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>TFTP</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>FTPS</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>OFTP</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>AFTP</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>SCP</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>AS2</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>WebDAV</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>WebDAVS</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Other DB's</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
 */
package com.github.elwinbran.javaoverhaul.repositories;
