/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.genericrepositories;

import com.github.elwinbran.jgrapi.hidden.Persistable;
import java.io.Serializable;
import java.util.Collection;

/**
 * The result of a 'create' operation of a repository that automatically assigns
 * indices to new entries/objects.
 * 
 * @author Elwin Slokker
 * 
 * @param <PrePersistableT> Class that only provides data and no index for an individual 
 * entry.
 * @param <IndexedPersistableT> Type of persistable that has an index and can be used in
 * the repository.
 * @param <IndexT> Index type the repository uses.
 */
public class CreationResult<
        PrePersistableT extends DataOnlyPersistable,
        IndexedPersistableT extends Persistable<? extends IndexT>,
        IndexT extends Serializable & Comparable<? extends IndexT>>
{
    /**
     * All objects that have been successfully persisted. 
     */
    private final Collection<IndexedPersistableT> persisted;
    
    /**
     * All objects that have been not been persisted.
     */
    private final Collection<PrePersistableT> skipped;
    
    /**
     * Simple default constructor.
     * 
     * @param persistedObjects
     * @param skippedObjects 
     */
    public CreationResult(Collection<IndexedPersistableT> persistedObjects,
                          Collection<PrePersistableT> skippedObjects)
    {
        this.persisted = persistedObjects;
        this.skipped = skippedObjects;
    }
    
    /**
     * @return The objects that were successfully stored and are now also 
     * objects that have an index and are thus removable and updatable.
     */
    public Collection<IndexedPersistableT> getPersistedObjects()
    {
        return this.persisted;
    }
    
    /**
     * @return The objects that could not be stored. Check the exceptions.
     */
    public Collection<PrePersistableT> getSkippedObjects()
    {
        return this.skipped;
    }
}
