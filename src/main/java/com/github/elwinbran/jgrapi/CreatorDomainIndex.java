/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.genericrepositories;

import com.github.elwinbran.jgrapi.hidden.Persistable;
import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.uei.ExceptionInformation;
import java.io.Serializable;
import java.util.Collection;

/**
 * Is able to create new objects in a data source.
 * 
 * @author Elwin Slokker
 * 
 * @param <PersistableT> Type of object that needs to be created.
 * @param <IndexT> The index type {@code P} uses to be unique.
 */
public interface CreatorDomainIndex<
        PersistableT extends Persistable<? extends IndexT>,
        IndexT extends Serializable & Comparable<? extends IndexT>>
{
    /**
     * Adds all {@code newTs} (or none when an exception occurs)to the 
     * repository.
     * <br><br>
     * 
     * @param newTs items/objects to be added.
     * @return If the returns {@code isEmpty == true} than the creation of 
     * {@code newTs} in the Repository has succeeded.
     * <br>Otherwise check the exceptions.
     */
    public abstract Collection<ExceptionInformation> atomicCreate(
            Collection<PersistableT> newTs);
    
    /**
     * Creates persistent entries from domain objects and skips those that could
     * not be created.
     * <br><br>
     * 
     * @param newTs items/objects to be added.
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be created.
     * <br>Refer to {@link SafeReturn the class itself} for the full 
     * specification.
     */
    public abstract SafeReturn<Collection<PersistableT>> create(
            Collection<? extends PersistableT> newTs);
}
