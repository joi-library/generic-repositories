/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.genericrepositories;

import com.github.elwinbran.jgrapi.hidden.Persistable;
import com.github.elwinbran.jerl.SafeReturn;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

/**
 * Persists objects in a storage that assign indices to objects automatically.
 * NOTE: Methods are named 'Add' to prevent name conflicts with {@link
 * CreatorDomainIndex}
 * 
 * @author Elwin Slokker
 * 
 * @param <PrePersistableT> Class that only provides data and no index for an individual 
 * entry.
 * @param <IndexedPersistableT> Type of persistable that has an index and can be used in
 * the repository.
 * @param <IndexT> Index type the repository uses.
 */
public interface CreatorIndexProviding<
        PrePersistableT extends DataOnlyPersistable,
        IndexedPersistableT extends Persistable<? extends IndexT>,
        IndexT extends Serializable & Comparable<? extends IndexT>>
{
    /**
     * Adds all {@code newTs} (or none when an exception occurs) to the 
     * repository.
     * <br><br>
     * Same as {@link #atomicCreate(java.util.Collection)} (this method calls 
     * that one under the hood).
     * 
     * @param newTs items/objects to be added.
     * @return If the returns {@code isEmpty == true} than the creation of 
     * {@code newTs} in the Repository has succeeded.
     * <br>Otherwise check the exceptions.
     */
    public default SafeReturn<Collection<IndexedPersistableT>>  atomicAdd(
            PrePersistableT... newTs)
    {
        return atomicAdd(Arrays.asList(newTs));
    }
    
    /**
     * Adds all {@code newTs} (or none when an exception occurs)to the 
     * repository.
     * <br><br>
     * 
     * @param newTs items/objects to be added.
     * @return If the returns {@code isEmpty == true} than the creation of 
     * {@code newTs} in the Repository has succeeded.
     * <br>Otherwise check the exceptions.
     */
    public SafeReturn<Collection<IndexedPersistableT>> atomicAdd(
            Collection<? extends PrePersistableT> newTs);
    
    /**
     * Creates persistent entries from domain objects and skips those that could
     * not be created.
     * <br><br>
     * Same as {@link #create(java.util.Collection)} (this method calls 
     * that one under the hood).
     * 
     * @param newTs items/objects to be added.
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be created.
     * <br>Refer to {@link SafeReturn the class itself} for the full 
     * specification.
     */
    public default SafeReturn<
        CreationResult<PrePersistableT, IndexedPersistableT, IndexT>>
         add(PrePersistableT... newTs)
    {
        return add(Arrays.asList(newTs));
    }
    
    /**
     * Creates persistent entries from domain objects and skips those that could
     * not be created.
     * <br><br>
     * 
     * @param newTs items/objects to be added.
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be created.
     * <br>Refer to {@link SafeReturn the class itself} for the full 
     * specification.
     */
    public SafeReturn<
        CreationResult<PrePersistableT, IndexedPersistableT, IndexT>>
         add(Collection<? extends PrePersistableT> newTs);
}
