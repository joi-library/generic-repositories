/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.genericrepositories;

import com.github.elwinbran.javaoverhaul.repositories.queries.DeletePredicate;
import com.github.elwinbran.jgrapi.hidden.Persistable;
import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.uei.ExceptionInformation;
import java.io.Serializable;
import java.util.Collection;

/**
 * Can delete entries/records/files from a storage.
 * 
 * @author Elwin Slokker
 * 
 * @param <PersistableT> Type of object that needs to be deleted.
 * @param <IndexT> The index type {@code T} uses to be unique.
 */
public interface Deleter<
        PersistableT extends Persistable< ? extends IndexT>,
        IndexT extends Serializable & Comparable<? extends IndexT>>
{
    /**
     * Deletes all {@code removedObjects} (or none when an exception occurs) 
     * from the repository.
     * <br><br>
     * 
     * @param removedObjects Items/objects to be deleted from the repository.
     * @return If the returns {@code isEmpty == true} than the deletion of 
     * {@code removedObjects} in the repository has succeeded.
     * <br>Otherwise check the exceptions to see what went wrong.
     */
    public abstract Collection<ExceptionInformation> atomicDelete(
            Collection<? extends PersistableT> removedObjects);
    
    /**
     * Deletes as much of {@code removedObjects} as possible.
     * 
     * @param removedObjects Items/objects to be deleted from the repository.
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be deleted (because of 
     * IO-issues).
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     */
    public abstract SafeReturn<Collection<PersistableT>> delete(
            Collection<? extends PersistableT> removedObjects);
    
    /**
     * Deletes as much of {@code removedObjects} as possible.
     * 
     * @param predicate Every entry/object in the storage is checked for this 
     * predicate and deleted if it is true.
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be deleted (because of 
     * IO-issues).
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     */
    public abstract SafeReturn<Collection<PersistableT>> delete(
            DeletePredicate<? extends PersistableT, ? extends IndexT> predicate);
}
