/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.genericrepositories;


import com.github.elwinbran.javaoverhaul.repositories.queries.ReadFilter;
import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jgrapi.hidden.Persistable;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

/**
 * Provides access to a collection of files or records of the same 'type'.
 * <br>For files, this objects reads all the specified objects from one folder.
 * <br>When accessing the type from a database, the whole column is available 
 * <br> for reading.
 * <br><br>
 * Architectural note: This class has been split from {@link Reader}.
 * <br>This was done because it is sometimes desirable to access a repository 
 * <br>only through reading. Otherwise, one would have to implement all CRUD
 * <br>methods to simply read some files, for instance.

 * 
 * @author Elwin Slokker
 * 
 * @param <PersistableT> Type of object that needs to be read/retrieved.
 * @param <IndexT> The index type {@code PersistableT} uses to be unique.
 */
public interface Filterable<
        PersistableT extends Persistable<? extends  IndexT>,
        IndexT extends Serializable & Comparable<? extends IndexT>>
        extends Reader<PersistableT, IndexT>
{ 
    
    /**
     * Reads or retrieves all files or records that qualify to be a {@code T}
     * and if not all can be read/retrieved, reads/retrieves none.
     * 
     * <br>Both lazy and eager implementation are allowed, so watch closely what
     * <br> implementation is used, because both can have surprising performance 
     * effects.
     * 
     * @param filter The extra argument that filters the entries/records read
     * from the storage.
     * @return An object that may carry the collection of read objects and/or 
     * <br> an exception message, when something went wrong.
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     * @deprecated Because I do not think atomic reads are an actual thing.
     * Of course could one need it, but I think other techniques exist to 
     * solve it.
     */
    public SafeReturn<Collection<PersistableT>> atomicRead(
            ReadFilter< ? extends PersistableT,? extends IndexT> filter);
    
    /**
     * Reads or retrieves all files or records that qualify to be a {@code T}.
     * <br>Both lazy and eager implementation are allowed, so watch closely what
     * <br> implementation is used, because both can have surprising performance 
     * effects.
     * 
     * @param filter The extra argument that filters the entries/records read
     * from the storage.
     * @return An object that may carry the collection of read objects and/or 
     * <br> an exception message, when something went wrong.
     * <br>Refer to {@link SafeReturn the class itself} for the full 
     * specification.
     */
    public SafeReturn<Collection<PersistableT>> read(
            ReadFilter<? extends PersistableT,? extends IndexT> filter);
    
    
    
    /**
     * Very WIP. Very interesting.
     * Obviously made to cater the needs of getting too many results from a repo.
     * @param filter The extra argument that filters the entries/records read
     * from the storage.
     * @param maxResultPerPage The maximum amount of read results in every
     * 'collection' or 'page'.
     * @return 
     */
    public default Iterator<SafeReturn<Collection<PersistableT>>> read(
            ReadFilter<? extends PersistableT, ? extends IndexT> filter, 
            final int maxResultPerPage)
    {
        return null;//TODO
    }
}
