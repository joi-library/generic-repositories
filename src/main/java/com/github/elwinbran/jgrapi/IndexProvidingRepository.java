/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.genericrepositories;

import com.github.elwinbran.javaoverhaul.repositories.ClearableRepository;
import com.github.elwinbran.jgrapi.hidden.Persistable;
import java.io.Serializable;

/**
 * Objects stored in this repository are the same type as the domain objects.
 * 
 * @author Elwin Slokker
 * 
 * @param <PersistableT>
 * @param <IndexT>
 */
public interface IndexProvidingRepository<
        PersistableT extends IndexedPersistable<? extends IndexT>,
        IndexT extends Serializable & Comparable<? extends IndexT>>
        extends CreatorDomainIndex<PersistableT, IndexT>,
                   Reader<PersistableT, IndexT>,
                   Updater<PersistableT, IndexT>,
                   Deleter<PersistableT, IndexT>,
                   ClearableRepository<PersistableT, PersistableT, IndexT>//little extra
{
    
}
