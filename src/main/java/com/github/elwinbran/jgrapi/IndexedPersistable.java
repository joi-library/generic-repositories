
package com.github.elwinbran.javaoverhaul.genericrepositories;

import com.github.elwinbran.jgrapi.hidden.Persistable;
import java.io.Serializable;

/**
 *
 * @author Elwin Slokker
 * @param <IndexT> The index type which makes this instance different from 
 * others (a primary key, if you like).
 */
public interface IndexedPersistable<
        IndexT extends Comparable<IndexT> & Serializable>
        extends Persistable<IndexT>
{
    
}
