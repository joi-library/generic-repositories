/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.genericrepositories;

import com.github.elwinbran.jgrapi.hidden.Persistable;
import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jgrapi.hidden.StandardPageIterator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

/**
 * Read access to a repository of a single 'type'.
 * 
 * Implementations provide the location/source of the repository.
 * The methods return all entries from the repository by default, but 
 * implementations may filter the result.
 * TODO this class can be both an atomic and non atomic service. But atomic 
 * reads might not exist just like you cannot query/filter a Create action.
 * 
 * @author Elwin Slokker
 * 
 * @param <PersistableT> Type of object that needs to be read/retrieved.
 * @param <IndexT> The index type {@code PersistableT} uses to be unique.
 */
public interface Reader<PersistableT extends 
        Persistable<? extends IndexT>, 
        IndexT extends Serializable & Comparable<? extends IndexT>>
{
    
    /**
     * Reads or retrieves all files or records that qualify to be a {@code T}
     * and if not all can be read/retrieved, reads/retrieves none.
     * 
     * <br>Both lazy and eager implementation are allowed, so watch closely what
     * <br> implementation is used, because both can have surprising performance 
     * effects.
     * 
     * @return An object that may carry the collection of read objects and/or 
     * <br> an exception message, when something went wrong.
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     * @deprecated Because I do not think atomic reads are an actual thing.
     * Of course could one need it, but I think other techniques exist to 
     * solve it.
     */
    public SafeReturn<Collection<PersistableT>> atomicRead();
    
    /**
     * Very WIP. Very interesting.
     * Obviously made to cater the needs of getting too many results from a repo.
     * @param maxResultPerPage
     * @return 
     */
    public default SafeReturn<Iterator<Collection<PersistableT>>> 
        atomicRead(int maxResultPerPage)
        {
            return null;//TODO
        }
    
    /**
     * Reads or retrieves all files or records that qualify to be a {@code T}.
     * <br>Both lazy and eager implementation are allowed, so watch closely what
     * <br> implementation is used, because both can have surprising performance 
     * effects.
     * 
     * @return An object that may carry the collection of read objects and/or 
     * <br> an exception message, when something went wrong.
     * <br>Refer to {@link SafeReturn the class itself} for the full 
     * specification.
     */
    public SafeReturn<Collection<PersistableT>> read();
    
    
    
    /**
     * Very WIP. Very interesting.
     * Obviously made to cater the needs of getting too many results from a repo.
     * @param maxResultPerPage
     * @return 
     */
    public default Iterator<SafeReturn<Collection<PersistableT>>> 
        read(int maxResultPerPage)
    {
        return new StandardPageIterator<>(read(), maxResultPerPage);
    }
}