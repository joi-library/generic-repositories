
package com.github.elwinbran.javaoverhaul.genericrepositories;

import java.io.Serializable;

/**
 *
 * @author Elwin Slokker
 * 
 * @param <PersistableT> Type of object that needs to be read/retrieved.
 * @param <IndexT> The index type {@code PersistableT} uses to be unique.
 */
public interface SimpleReader<
        PersistableT extends IndexedPersistable<? extends IndexT>,
        IndexT extends Serializable & Comparable<? extends IndexT>>
        extends Reader<PersistableT, IndexT>
{
    
}
