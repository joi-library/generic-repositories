/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.genericrepositories;

import com.github.elwinbran.javaoverhaul.repositories.queries.UpdateStatement;
import com.github.elwinbran.jgrapi.hidden.Persistable;
import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.uei.ExceptionInformation;
import java.io.Serializable;
import java.util.Collection;

/**
 * Is able to updates entries/records/files in a storage.
 * The index is used as a reference.
 * 
 * @author Elwin Slokker
 * 
 * @param <PersistableT> Type of object that needs to be updated.
 * @param <IndexT> The index type {@code PersistableT} uses to be unique.
 */
public interface Updater<
        PersistableT extends Persistable<? extends IndexT>,
        IndexT extends Serializable & Comparable<? extends IndexT>>
{
    /**
     * Updates all {@code newVersions} (or none when an exception occurs) in the 
     * repository.
     * <br><br>
     * 
     * @param newVersions items/objects to be updated.
     * @return If the returns {@code isEmpty == true} than the creation of 
     * {@code newTs} in the Repository has succeed.
     * <br>Otherwise check the exceptions.
     */
    public Collection<ExceptionInformation> 
        atomicUpdate(Collection<? extends PersistableT> newVersions);
    
    /**
     * Finds the older versions of the {@code newVersions} in the repository
     * <br>and updates them, skipping objects when unable to update.
     * <br><br>
     * 
     * @param newVersions The replacement objects/items.
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be updated.
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     */    
    public abstract SafeReturn<Collection<PersistableT>> 
        update(Collection<? extends PersistableT> newVersions);
        
    /**
     * Updates all {@code newVersions} (or none when an exception occurs) in the 
     * repository.
     * <br><br>
     * 
     * @param statement The thing that is executed for every record/entry/object
     * in the storage (but this also includes a check, so some 
     * records/entries/objects are unaffected).
     * @return If the returns {@code isEmpty == true} than the creation of 
     * {@code newTs} in the Repository has succeed.
     * <br>Otherwise check the exceptions.
     */
    public Collection<ExceptionInformation> atomicUpdate(
           UpdateStatement<? extends PersistableT, ? extends IndexT> statement);
    
    /**
     * Finds the older versions of the {@code newVersions} in the repository
     * <br>and updates them, skipping objects when unable to update.
     * <br><br>
     * 
     * @param statement The thing that is executed for every record/entry/object
     * in the storage (but this also includes a check, so some 
     * records/entries/objects are unaffected).
     * @return An object that may carry an exception and/or a set of objects, 
     * <br>those indicate the objects that could not be updated.
     * <br>Refer to {@see ExceptionReturnable the class itself} for the full 
     * specification.
     */    
    public abstract SafeReturn<Collection<PersistableT>> update(
           UpdateStatement<? extends PersistableT, ? extends IndexT> statement);
}
