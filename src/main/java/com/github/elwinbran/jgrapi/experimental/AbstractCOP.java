/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.jgrapi.experimental;

/**
 *
 * @author Elwin Slokker
 * @param <T>
 */
public abstract class AbstractCOP<T> implements ConstrainedObjectProperty<T>
{

    /**
     * Does not take constraints in consideration.
     * @param other
     * @return 
     */
    @Override
    public int compareTo(ConstrainedObjectProperty<?> other)
    {
        if(other == null)
        {
            throw new NullPointerException("Cannot compare null");
        }
        if(this == other)
        {
            return 0;
        }
        if(this.getName().equals(other.getName()))
        {
            if(this.propertyType() == other.propertyType())
            {
                return 0;
            }
            return this.propertyType().getCanonicalName().compareTo(
                    other.propertyType().getCanonicalName());
        }
        return this.getName().compareTo(other.getName());
    }
    
}
