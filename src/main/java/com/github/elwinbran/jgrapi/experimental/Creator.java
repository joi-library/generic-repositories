/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.jgrapi.experimental;

import java.util.Map;

/**
 * Creates objects of the specified type.
 * Must do so without reflection and create a fully usable object.
 * This class is useful for deserialization since a generic deserializer only 
 * requires this class as abstract object creation.
 * 
 * @author Elwin Slokker
 * @param <ObjectT> Type of object created.
 * @param <ValueBoundT> The allowed types of values for creation.
 */
public interface Creator<ObjectT, ValueBoundT>
{
    /**
     * Takes all essential properties required to build a new instance of
     * {@code ObjecT}.
     * 
     * @param properties A map of the required properties to create the object.
     * @return a new instance of {@code ObjecT}.
     * @deprecated 
     */
    @Deprecated
    public abstract ObjectT create(Map<String, ValueBoundT> properties);
    
    /**
     * Takes all essential properties required to build a new instance of
     * {@code ObjecT}.
     * 
     * @param base
     * @return a new instance of {@code ObjecT}.
     */
    public abstract ObjectT create(TruePersistable base);
    
}
