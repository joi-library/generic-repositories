/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.jgrapi.experimental;

import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jgrapi.GenericInterpreter;
import java.io.Serializable;

/**
 * Takes any persistable that can expose its properties and serializes it as
 * JSON, alternatively takes JSON and produces an object.
 * 
 * @author Elwin Slokker
 */
public class GenericJSONTranslator<IndexT extends Serializable & 
                                                  Comparable<IndexT>>
        extends GenericInterpreter<TruePersistable<IndexT>, IndexT, String>
{

    @Override
    public SafeReturn<TruePersistable<IndexT>> interpret(String data)
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public SafeReturn<String> marshal(TruePersistable<IndexT> object)
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }
    
}
