/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.jgrapi.experimental;

import com.github.elwinbran.jgrapi.redesign.PersistableReference;
import javafx.util.Pair;

/**
 * An object that exposes the properties it owns. 
 * Composed (non-primitive) objects are exposed through a reference, that
 * contains the {@link PersistableReference#getSource storage} as well as the
 * {@link PersistableReference#getConstraints class constraints}.
 * 
 * @author Elwin Slokker
 */
public interface NestablePropertyExposer
{
    public abstract Iterable<Pair<String, PrimitiveSerializable>> 
        getPrimitives();
    
    public abstract Iterable<Pair<String, PersistableReference>> 
        getComposedObjects();
}
