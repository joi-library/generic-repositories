/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.jgrapi.experimental;


/**
 * A wrapper object that can only contain a primitive object or an iterable of
 * primitive objects.
 * 
 * This class should not be extended, because that allows hacks to insert 
 * non-primitive values, which are not by default serializable.
 * 
 * TODO doc to finish.
 * 
 * @author Elwin Slokker
 */
public final class PrimitiveSerializable implements SerialValue
{
    /**
     * Keeps the primitive (or reference to array)
     */
    private final Object value;
    
    /**
     * Hidden constructor that simply saves the value.
     * 
     * @param object Anything, but it should be a 'primitive'.
     */
    private PrimitiveSerializable(final Object object)
    {
        value = object;
    }
    
    public static PrimitiveSerializable make(final Byte number)
    {
        return new PrimitiveSerializable(number);
    }
    
    public static PrimitiveSerializable make(final Short number)
    {
        return new PrimitiveSerializable(number);
    }
    
    public static PrimitiveSerializable make(final Integer number)
    {
        return new PrimitiveSerializable(number);
    }
    
    public static PrimitiveSerializable make(final Float number)
    {
        return new PrimitiveSerializable(number);
    }
    
    public static PrimitiveSerializable make(final Long number)
    {
        return new PrimitiveSerializable(number);
    }
    
    public static PrimitiveSerializable make(final Double number)
    {
        return new PrimitiveSerializable(number);
    }
    
    public static PrimitiveSerializable make(final String string)
    {
        return new PrimitiveSerializable(string);
    }
    
    public static PrimitiveSerializable make(final Character character)
    {
        return new PrimitiveSerializable(character);
    }
    
    public static PrimitiveSerializable make(final Boolean statement)
    {
        return new PrimitiveSerializable(statement);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final boolean[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final Boolean[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final byte[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final Byte[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final char[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final Character[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final short[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final Short[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final Integer[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final Float[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final Long[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static PrimitiveSerializable make(final Double[] array)
    {
        return new PrimitiveSerializable(array);
    }
    
    /**
     * Takes the array of Strings and concats it to one very long string.
     * @param lines
     * @return 
     */
    public static PrimitiveSerializable make(final String[] lines)
    {
        return new PrimitiveSerializable(lines);
    }
    
    /**
     * Convenience method that concats all strings into one.
     * 
     * @param lines A sequence of {@code String} that contains at least one 
     * element.
     * @return A primitive that contains a {@code String}.
     */
    public static PrimitiveSerializable makeWithStrings(
            final Iterable<String> lines)
    {
        return new PrimitiveSerializable(lines);
    }
    
    public static PrimitiveSerializable makeWithBoolean(
            final Iterable<Boolean> statements)
    {
        return new PrimitiveSerializable(statements);
    }
    
    public static PrimitiveSerializable makeWithCharacter(
            final Iterable<Character> chars)
    {
        return new PrimitiveSerializable(chars);
    }
    
    public static PrimitiveSerializable makeWithBytes(
            final Iterable<Byte> numbers)
    {
        return new PrimitiveSerializable(numbers);
    }
    
    public static PrimitiveSerializable makeWithShorts(
            final Iterable<Short> numbers)
    {
        return new PrimitiveSerializable(numbers);
    }
    
    public static PrimitiveSerializable makeWithIntegers(
            final Iterable<Integer> numbers)
    {
        return new PrimitiveSerializable(numbers);
    }
    
    public static PrimitiveSerializable makeWithFloats(
            final Iterable<Float> numbers)
    {
        return new PrimitiveSerializable(numbers);
    }
    
    public static PrimitiveSerializable makeWithLongs(
            final Iterable<Long> numbers)
    {
        return new PrimitiveSerializable(numbers);
    }
    
    public static PrimitiveSerializable makeWithDoubles(
            final Iterable<Double> numbers)
    {
        return new PrimitiveSerializable(numbers);
    }

    /**
     * Checks if this serial value is the same type as an other.
     * 
     * @param other another primitive value.
     * @return {@code true} when this serial value is of the same type as the 
     * given value and {@code false} otherwise.
     */
    public boolean isSameType(PrimitiveSerializable other)
    {
        return isSameType(other.value.getClass());
    }
    
    /**
     * Checks if this serial value is the same type as a given class.
     * 
     * @param type The class/type to be used as check. Should be a Primitive.
     * @return {@code true} when this serial value is of the same type as the 
     * given type and {@code false} otherwise.
     */
    public boolean isSameType(Class<?> type)
    {
        return this.value.getClass().equals(type);
    }
    
    /**
     * 
     * @return Ensured to be a primitive or an array/iterable of primitives.
     */
    @Override
    public Object getValue()
    {
        return this.value;
    }
}
