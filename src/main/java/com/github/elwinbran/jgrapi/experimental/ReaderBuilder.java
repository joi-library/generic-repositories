
package com.github.elwinbran.jgrapi.experimental;

import com.github.elwinbran.jgrapi.hidden.Persistable;
import com.github.elwinbran.jgrapi.Reader;
import java.io.Serializable;
import java.net.URI;
import java.net.URL;

/**
 *
 * @author Elwin Slokker
 * 
 * @param <DomainT> Domain object type that {@code PersistableT} represents.
 * @param <PersistableT> Type of object that needs to be read/retrieved.
 * @param <IndexT> The index type {@code PersistableT} uses to be unique.
 */
public abstract class ReaderBuilder<DomainT extends Object,
        PersistableT extends Persistable<DomainT, IndexT>,
        IndexT extends Serializable & Comparable<IndexT>>
{
    //set
    /**
     * Set all required properties of a data source through an url.
     * <br>Makes other setters unnecessary.
     * <br>url provides location, protocol, authentication, optional parameters.
     * 
     * @param url
     * @deprecated Could be used in the future.
     */
    @Deprecated
    public abstract void setFullSource(URL url);
    
    /**
     * Set all required properties of a data source through an uri.
     * <br>Makes other setters unnecessary.
     * <br>uri provides location, protocol, authentication, optional parameters.
     * 
     * @param uri 
     * @deprecated Could be used in the future.
     */
    @Deprecated
    public abstract void setFullSource(URI uri);
    
    public abstract void setPath(String path);
    
    /**
     * TODO maybe return read queryable
     * @return 
     */
    public abstract Reader<DomainT, PersistableT, IndexT> build();
}
