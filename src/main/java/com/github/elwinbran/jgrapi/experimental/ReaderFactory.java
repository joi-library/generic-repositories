/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.jgrapi.experimental;

import com.github.elwinbran.jgrapi.IndexedPersistable;
import com.github.elwinbran.jgrapi.hidden.Persistable;
import com.github.elwinbran.jgrapi.Reader;
import com.github.elwinbran.jgrapi.ReaderQueryable;
import com.github.elwinbran.jgrapi.SimpleReader;
import com.github.elwinbran.jgrapi.experimental.wrapped.WrappingPersistable;
import com.github.elwinbran.jgrapi.experimental.wrapped.WrappingReaderQueryable;
import com.github.elwinbran.jgrapi.experimental.wrapped.WrappingSimpleReader;
import java.io.Serializable;

/**
 * as WIP as possible
 * TODO
 * @author Elwin Slokker
 * 
 * @deprecated Interesting idea but it probably too hard or messy to realize.
 */
@Deprecated
public abstract class ReaderFactory 
{
    private static ReaderFactory INSTANCE = null;
    public static ReaderFactory getInstance()
    {
        if(INSTANCE == null)
        {
            //instantiate
        }
        return INSTANCE;
    }
    
    public abstract <PersistableT extends IndexedPersistable<IndexT>,
                     IndexT extends Serializable & Comparable<IndexT>>
        SimpleReader<PersistableT, IndexT> 
        makeSimpleReader(WrappingProvidedCommand<PersistableT, IndexT> order);
        
    public abstract <PersistableT extends IndexedPersistable<IndexT>,
                     IndexT extends Serializable & Comparable<IndexT>>
        ReaderQueryable<PersistableT, IndexT> 
        makeReaderQueryable(WrappingProvidedCommand order);
        
    public abstract <DomainT extends Object,
        PersistableT extends WrappingPersistable<DomainT, IndexT>,
        IndexT extends Serializable & Comparable<IndexT>>
        WrappingSimpleReader<DomainT, PersistableT, IndexT>
        makeWrappingSimpleReader(WrappingProvidedCommand order);
        
    public abstract <DomainT extends Object,
        PersistableT extends WrappingPersistable<DomainT, IndexT>,
        IndexT extends Serializable & Comparable<IndexT>>
        WrappingReaderQueryable<DomainT, PersistableT, IndexT>
        makeWrappingReaderQueryable(WrappingProvidedCommand order,
                ReaderBuilder<DomainT, PersistableT, IndexT> builder);
    
    
    public <DomainT extends Object,
        PersistableT extends Persistable<DomainT, IndexT>,
        IndexT extends Serializable & Comparable<IndexT>>
        Reader<DomainT, PersistableT, IndexT>
        makeReader(WrappingProvidedCommand order)
        {
            final ReaderBuilder<DomainT, PersistableT, IndexT> builder;
            //first make reader based on format
            switch(order.getFormat())
            {
                case JSON: builder = new TwoStepReaderBuilder<>(null); break;
                default: builder = null; break;
            }
            return makeReader(order, builder);
        }
    
    private <DomainT extends Object,
        PersistableT extends Persistable<DomainT, IndexT>,
        IndexT extends Serializable & Comparable<IndexT>>
        Reader<DomainT, PersistableT, IndexT>
        makeReader(WrappingProvidedCommand order, 
                   ReaderBuilder<DomainT, PersistableT, IndexT> builder)
        {
            return builder.build();
        }
}
