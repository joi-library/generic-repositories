
package com.github.elwinbran.jgrapi.experimental;

import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jgrapi.GenericInterpreter;
import com.github.elwinbran.jgrapi.hidden.Persistable;
import com.github.elwinbran.jgrapi.Reader;
import com.github.elwinbran.jgrapi.hidden.TwoStepReader;
import java.io.Serializable;
import java.net.URI;
import java.net.URL;
import java.util.Collection;

/**
 *
 * @author Elwin Slokker
 */
public class TwoStepReaderBuilder <DomainT extends Object,
        PersistableT extends Persistable<DomainT, IndexT>,
        IndexT extends Serializable & Comparable<IndexT>,
        DataT extends Object>
        extends ReaderBuilder<DomainT, PersistableT, IndexT>
{

    public TwoStepReaderBuilder(GenericInterpreter<PersistableT, IndexT, DataT>
            interpreter)
    {
        
    }
    
    @Override
    public void setFullSource(URL url) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setFullSource(URI uri) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Reader<DomainT, PersistableT, IndexT> build()
    {
        return null;//new TwoStepReader();
    }
}
