
package com.github.elwinbran.jgrapi.experimental;

/**
 *
 * @author Elwin Slokker
 */
public abstract class WrappingProvidedCommandBuilder
{
    
    public abstract WrappingProvidedCommand build();
}
