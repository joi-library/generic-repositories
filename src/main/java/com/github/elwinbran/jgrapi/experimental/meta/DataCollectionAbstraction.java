
package com.github.elwinbran.jgrapi.experimental.meta;


/**
 *
 * @author Elwin Slokker
 */
public interface DataCollectionAbstraction
{
    public DataFormat getUnderlyingFormat();
    public Implementation getImplementation();
    public Quality getQuality();
}
