
package com.github.elwinbran.jgrapi.experimental.meta;

/**
 * Use to specify what format is the input or output of a DAO.
 * 
 * @author Elwin Slokker
 */
public enum DataFormat
{
    /**
     * Data is read from a JSON (JavaScriptObjectNotation) file or string.
     */
    JSON,
    /**
     * Data is read from an XML (Extensible Markup Language) file or string.
     */
    XML,
    /**
     * The data has no format and is provided directly, like from an API (and
     * that may be a Database).
     */
    VIRTUAL
}
