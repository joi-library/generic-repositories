
package com.github.elwinbran.jgrapi.experimental.meta;

/**
 *
 * @author Elwin Slokker
 */
public enum DataKind
{
    BYTES,
    STRING
}
