
package com.github.elwinbran.jgrapi.experimental.meta;

/**
 * Marks the kind of implementation.
 * 
 * @author Elwin Slokker
 */
public enum Implementation
{
    /**
     * Marks a 'lazy' implementation that only does something at the last moment.
     */
    LAZY,
    
    /**
     * Marks an 'eager' implementation that does all the work as soon as it can.
     */
    EAGER
}
