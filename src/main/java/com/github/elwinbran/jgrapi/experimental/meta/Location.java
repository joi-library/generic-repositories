
package com.github.elwinbran.jgrapi.experimental.meta;

/**
 *
 * @author Elwin Slokker
 */
public enum Location
{
    LOCAL,
    LAN,
    CORPORATE_NETWORK,
    WORLD_WIDE_WEB
}
