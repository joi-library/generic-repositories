
package com.github.elwinbran.jgrapi.experimental.meta;

/**
 * How the service is provided. 
 * Does not necessarily have to do with 'quality'.
 * The service could be a all-or-nothing or have another quality model.
 * @author Elwin Slokker
 */
public enum Quality
{
    /**
     * The service tries to fully meet a request, but ignores parts when unable 
     * to comply. This causes incomplete requests and the service will tell you
     * what could not be completed.
     */
    BEST_EFFORT,
    /**
     * The service meets the ACID (Atomic, Consistency, Isolated, Durable) 
     * requirements.
     * This also means a request can either succeed or fail, no in between.
     * The downside is of course that more resources are used and requests
     * take more time.
     */
    RELIABLE
}
