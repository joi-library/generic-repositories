
package com.github.elwinbran.jgrapi.experimental.meta;

/**
 * Provides information about services.
 * 
 * @author Elwin Slokker
 */
public interface ServiceQualityCheckable
{
    public boolean isAtomic();
}
