
package com.github.elwinbran.jgrapi.experimental.meta;

/**
 *
 * @author Elwin Slokker
 */
public enum Source
{
    FILE_SYSTEM,
    RELATIONAL_DATABASE,
    NOSQL_DATABASE,
    OBJECT_DATABASE,
    UNKNOWN
}
