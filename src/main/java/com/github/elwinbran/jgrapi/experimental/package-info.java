/**
 * Stuff.
 * If {@code Car} inherits {@code TruePersistable} there must exist a singleton 
 * that implements {@code DataMap<Car>} ({@code == OrderedProperties : 
 * Iterable<ConstrainedObjectProperty>}). {@code DataMaps} is a singleton map 
 * that keeps track of all datamaps.
 * 
 * New thing is to make it all with nestables.
 * So nestable constraints and properties. Repos and creators will have to deal 
 * with nesting.
 */
package com.github.elwinbran.jgrapi.experimental;
