/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.jgrapi.hidden;

import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jgrapi.ClearableRepository;
import com.github.elwinbran.jgrapi.CreationResult;
import com.github.elwinbran.jgrapi.DataOnlyPersistable;
import com.github.elwinbran.jgrapi.DeletePredicate;
import com.github.elwinbran.jgrapi.DeleteQueryBuilder;
import com.github.elwinbran.jgrapi.Deleter;
import com.github.elwinbran.jgrapi.RUDQueryable;
import com.github.elwinbran.jgrapi.ReadQueryBuilder;
import com.github.elwinbran.jgrapi.UpdateQueryBuilder;
import com.github.elwinbran.jgrapi.UpdateStatement;
import com.github.elwinbran.jgrapi.Updater;
import com.github.elwinbran.jgrapi.experimental.TruePersistable;
import com.github.elwinbran.uei.ExceptionInformation;
import java.io.Serializable;
import java.util.Collection;
import com.github.elwinbran.jgrapi.experimental.ConstrainedProperties;
import com.github.elwinbran.jgrapi.CreatorIndexProviding;
import com.github.elwinbran.jgrapi.CreatorDomainIndex;

/**
 * An abstract class that should be inherited for every generic repository implementation.
 * 
 * @author Elwin Slokker
 * @param <DomainT> Domain object type.
 * @param <PrePersistableT> No index persistable type.
 * @param <PersistableT> Type of object that needs to stored.
 * @param <IndexT> The index type {@code T} uses to be unique.
 */
public abstract class GenericRepository<DomainT extends Object,
        PrePersistableT extends TruePersistable<? extends DomainT, IndexT>,
        PersistableT extends TruePersistable<? extends DomainT,
                                             ? extends IndexT>,
        IndexT extends Serializable & Comparable<? extends IndexT>>
        implements CreatorDomainIndex<DomainT, PersistableT, IndexT>,
                   CreatorIndexProviding<DomainT, PrePersistableT, 
                                        PersistableT, IndexT>,
                   Updater<DomainT, PersistableT, IndexT>,
                   Deleter<DomainT, PersistableT, IndexT>,
                   RUDQueryable<DomainT, PersistableT, IndexT>,
                   ClearableRepository<DomainT, PrePersistableT, 
                                       PersistableT, IndexT>
{
    /**
     * Tells how a stream of data should be interpreted.
     */
    private final ConstrainedProperties specification;
    
    public GenericRepository(ConstrainedProperties objectSpecification)
    {
        this.specification = objectSpecification;
    }
    
    @Override
    public Collection<ExceptionInformation> 
        atomicCreate(Collection<PersistableT> newTs)
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public SafeReturn<Collection<PersistableT>> 
        create(Collection<? extends PersistableT> newTs)
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public SafeReturn<Collection<PersistableT>> 
        atomicAdd(Collection<? extends PrePersistableT> newTs)
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public SafeReturn<
        CreationResult<DomainT, PrePersistableT, PersistableT, IndexT>>
        add(Collection<? extends PrePersistableT> newTs)
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public Collection<ExceptionInformation>
        atomicUpdate(Collection<? extends PersistableT> newVersions)
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public SafeReturn<Collection<PersistableT>>
         update(Collection<? extends PersistableT> newVersions)
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public Collection<ExceptionInformation>
        atomicDelete(Collection<? extends PersistableT> removedObjects)
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public SafeReturn<Collection<PersistableT>>
        delete(Collection<? extends PersistableT> removedObjects)
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }
        
    @Override
    public Updater<DomainT, PersistableT, IndexT> newUpdateQueryBuilder(
            UpdateStatement<? extends DomainT, ? extends PersistableT,
                            ? extends IndexT> statement) {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public Deleter<DomainT, PersistableT, IndexT> newDeleteQuery(
            DeletePredicate<? extends DomainT, ? extends PersistableT, 
                            ? extends IndexT> clause) {
        throw new UnsupportedOperationException("This is a null-implementation.");

    @Override
    public ReadQueryBuilder<DomainT, PersistableT, IndexT> newReadQueryBuilder() {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public SafeReturn<Collection<PersistableT>> atomicRead()
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public SafeReturn<Collection<PersistableT>> read()
    {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public SafeReturn<Boolean> clear() {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    
    }
    
}
