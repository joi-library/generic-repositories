/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.jgrapi.hidden;

import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jerl.SafeReturnConstructor;
import com.github.elwinbran.jgrapi.ReadQueryBuilder;
import com.github.elwinbran.uei.BaseExceptions;
import com.github.elwinbran.uei.ExceptionInformation;
import com.github.elwinbran.uei.ExceptionInformationBuilder;
import com.github.elwinbran.uei.IOExceptions;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import com.github.elwinbran.jgrapi.Filterable;

/**
 * Deserializes files in the local filesystem.
 * 
 * @author Elwin Slokker
 * 
 * @param <DomainT> Type of domain object that is persisted.
 * @param <PersistableT> Type of object that needs to be read/retrieved.
 * @param <IndexT> The index type {@code PersistableT} uses to be unique.
 */
public class LocalSerialReader<DomainT extends Object,
        PersistableT extends Persistable<DomainT, IndexT>,
        IndexT extends Serializable & Comparable<IndexT>>
        implements Filterable<DomainT, PersistableT, IndexT>
{
    private final File targetDirectory;
    
//==============================================================================
//Constructors
//==============================================================================
    public static<DomainT extends Object,
        PersistableT extends Persistable<DomainT, IndexT>,
        IndexT extends Serializable & Comparable<IndexT>> 
        SafeReturn<LocalSerialReader<DomainT,PersistableT,IndexT>> 
        make(File directory)
    {
        if(!directory.exists())
        {
            ExceptionInformationBuilder builder = ExceptionInformationBuilder.newBuilder();
            builder.setMessage(directory.toString() + " does not exist.");
            builder.setLevel(Level.WARNING);
            builder.setThrower(LocalSerialReader.class);
            builder.setType(IOExceptions.FILE_NOT_FOUND);
            return new SafeReturnConstructor().construct(builder.build());
        }
        else
        {
            return new SafeReturnConstructor().construct(
                    new LocalSerialReader(directory));
        }
    }
        
    public static<DomainT extends Object,
        PersistableT extends Persistable<DomainT, IndexT>,
        IndexT extends Serializable & Comparable<IndexT>> 
        SafeReturn<LocalSerialReader<DomainT,PersistableT,IndexT>> 
        make(URL repositoryLocator)
    {
        File target = new File(repositoryLocator.getPath());
        if(!target.exists())
        {
            ExceptionInformationBuilder builder = ExceptionInformationBuilder.newBuilder();
            builder.setMessage(target.toString() + " does not exist.");
            builder.setLevel(Level.WARNING);
            builder.setThrower(LocalSerialReader.class);
            builder.setType(IOExceptions.FILE_NOT_FOUND);
            return new SafeReturnConstructor().construct(builder.build());
        }
        else
        {
            return new SafeReturnConstructor().construct(
                    new LocalSerialReader(target));
        }
    }
    
    public LocalSerialReader(File directory)
    {
        this.targetDirectory = directory;
    }
    
    public LocalSerialReader(URL repositoryLocator)
    {
        checkURL(repositoryLocator);
        targetDirectory = null;
    }

    private void checkURL(URL repositoryLocator)
    {
        if(!repositoryLocator.getProtocol().equals("file") ||
                !new File(repositoryLocator.getPath()).exists())
        {
            throw new IllegalArgumentException("URL does not point to "
                    + "existing local file");
        }
    }
    
//==============================================================================
//Implemented Methods
//==============================================================================
    
    /**
     * Reads all the files from the directory and returns the collection of 
     * objects it was able to make.
     * <br> It will return all objects in read order, not index.
     * TODO: write shorter.
     * 
     * @return 
     */
    @Override
    public SafeReturn<Collection<PersistableT>> read()
    {
        final Collection<ExceptionInformation> exceptions = new LinkedList<>();
        Collection<PersistableT> readObjects = null;
        if(this.targetDirectory.isDirectory())
        {
            final File[] files = this.targetDirectory.listFiles();
            final List<File> targetFiles = new ArrayList<>(files.length / 2);
            for(File f : files)
            {
                if(f.isFile())
                {
                    targetFiles.add(f);
                }
            }
            if(targetFiles.isEmpty())
            {
                ExceptionInformationBuilder builder = 
                        ExceptionInformationBuilder.newBuilder();
                builder.setMessage("No files found at:\n" + 
                            this.targetDirectory.getAbsolutePath());
                builder.setLevel(Level.WARNING);
                builder.setThrower(this);
                exceptions.add(builder.build());
                readObjects = Collections.EMPTY_LIST;
            }
            else
            {
                readObjects = new ArrayList<>(targetFiles.size());
                for(File f : targetFiles)
                {
                    SafeReturn<PersistableT>  
                            deserializeResult = readSingleFile(f);
                    if(deserializeResult.hasReturnValue())
                    {
                        readObjects.add(deserializeResult.getReturn());
                    }
                    else if(!deserializeResult.getExceptions().isEmpty())
                    {
                        for(ExceptionInformation info : deserializeResult.getExceptions())
                        {
                            exceptions.add(info);
                        }
                    }
                }
            }
        }
        else
        {
            ExceptionInformationBuilder builder = 
                        ExceptionInformationBuilder.newBuilder();
            builder.setMessage("This reader can only use directories.");
            builder.setLevel(Level.WARNING);
            builder.setThrower(this);
            exceptions.add(builder.build());
        }
        //if read is empty...
        return new SafeReturnConstructor<Collection<PersistableT>>().construct(exceptions);
    }
    
    @Override
    public SafeReturn<Collection<PersistableT>> atomicRead()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ReadQueryBuilder<DomainT, PersistableT, IndexT> newReadQueryBuilder()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
//==============================================================================
//Helper Methods
//==============================================================================

    /**
     * 
     * note: bullshit huge method, because mapping is likely impossible, given
     * the need for maintainability.
     * 
     * @param targetFile
     * @return 
     */
    public SafeReturn<PersistableT> readSingleFile(File targetFile)
    {
        ExceptionInformationBuilder builder = 
                    ExceptionInformationBuilder.newBuilder();
        ExceptionInformation information = null;
        builder.setLevel(Level.SEVERE);
        builder.setThrower(this); 
        PersistableT deserializedObject = null;
        try {
            InputStream file = new FileInputStream(targetFile);
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream(buffer);
            deserializedObject = (PersistableT) input.readObject();
            input.close();
        } catch (FileNotFoundException ex){
            //TODO remove unfound file from index
            builder.setType(IOExceptions.FILE_NOT_FOUND);
            builder.setMessage("Target file could not be found, printing"
                    + "details on next line:\n" + targetFile.toString() + "\n");
            builder.setThrowable(ex);
            information = builder.build();
        } catch (InterruptedIOException ex){
            builder.setType(IOExceptions.INTERUPTED_IO);
            builder.setMessage("File reading was externally shut down; review"
                    + "if following file is in use:\n" + targetFile.toString() +
                    "\n");
            builder.setThrowable(ex);
            information = builder.build();
        } catch (IOException ex) {
            builder.setType(IOExceptions.IO);
            builder.setMessage("Something went wrong while reading the file.");
            builder.setThrowable(ex);
            information = builder.build();
        } catch (ClassNotFoundException ex){
            //likely a version error or the caller gave the wrong directory
            builder.setType(BaseExceptions.CLASS_NOT_FOUND);
            builder.setMessage("The read file did not contain a known class.");
            builder.setThrowable(ex);
            information = builder.build();
        }
        return util(deserializedObject, information);
    }
    
    private SafeReturn<PersistableT> util(PersistableT object, 
                                          ExceptionInformation information)
    {
        if(object == null)
        {
            return new SafeReturnConstructor<PersistableT>().construct(information);
        }
        return new SafeReturnConstructor<PersistableT>().construct(object);
    }

    
}
