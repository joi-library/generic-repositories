
package com.github.elwinbran.jgrapi.hidden;

import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jerl.SafeReturnConstructor;
import com.github.elwinbran.jgrapi.DataReader;
import com.github.elwinbran.uei.ExceptionInformation;
import com.github.elwinbran.uei.ExceptionInformationBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

/**
 * Capable of reading files on the local machine.
 * 
 * @author Elwin Slokker
 */
public class LocalTextFileReader implements DataReader<List<String>>
{
    private final File source;
    
    public LocalTextFileReader(final File file)
    {
        this.source = file;
    }
    
    /**
     * Reads data in the form of Strings from the source specified at creation.
     * 
     * @return A wrapper object that may carry the read strings and/or 
     * exception(s), if anything went wrong. 
     * <br>Refer to {@link SafeReturn the class itself} for the full 
     * specification.
     */
    @Override
    public SafeReturn<List<String>> readSource()
    {
        List<String> lines = null;
        ExceptionInformation[] exceptions = null;
        try(BufferedReader reader = new BufferedReader(
                new FileReader(this.source)))
        {
            lines = new ArrayList<>(100);
            String line = reader.readLine();
            while(line != null)
            {
                lines.add(line);
                line = reader.readLine();
            }
            reader.close();
        }
        catch(Throwable exception)
        {
            ExceptionInformationBuilder builder = 
                    ExceptionInformationBuilder.newBuilder();
            builder.setMessage(exception.getMessage());
            builder.setLevel(Level.SEVERE);
            builder.setThrowable(exception);
            builder.setThrower(this);
            exceptions = new ExceptionInformation[]{builder.build()};
        }
        return new SafeReturnConstructor<List<String>>().
                construct(lines, exceptions);
    }
}
