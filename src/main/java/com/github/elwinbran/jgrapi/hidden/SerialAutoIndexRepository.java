
package com.github.elwinbran.jgrapi.hidden;

import com.github.elwinbran.jgrapi.hidden.Persistable;
import com.github.elwinbran.jgrapi.hidden.LocalSerialReader;
import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jerl.SafeReturnConstructor;
import com.github.elwinbran.jgrapi.CreationResult;
import com.github.elwinbran.jgrapi.DataOnlyPersistable;
import com.github.elwinbran.uei.ExceptionInformation;
import com.github.elwinbran.uei.ExceptionInformationBuilder;
import com.github.elwinbran.uei.IOExceptions;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import com.github.elwinbran.jgrapi.DomainIndexRepository;

/**
 * Manages serialised objects on the local filesystem. Assigns number index on 
 * creation of an object.
 * TODO finish doc and overall code.
 * @author Elwin Slokker
 * 
 * @param <PrePersistableT> Class that only provides data and no index for an individual 
 * entry.
 * @param <IndexedPersistableT> Type of persistable that has an index and can be used in
 * the repository.
 * @param <IndexT> Index type the repository uses.
 */
public class SerialAutoIndexRepository<
        PrePersistableT extends DataOnlyPersistable<Object, IndexT>,
        IndexedPersistableT extends Persistable<Object, IndexT>,
        IndexT extends Serializable & Comparable<IndexT>>
        extends DomainIndexRepository<PrePersistableT, IndexedPersistableT, IndexT>
{
    private final ReadableRepository<Object, IndexedPersistableT, IndexT> reader;
    private final File targetDirectory;
    private List<IndexT> indices;//TODO make final
    //private List<T> cache;
    
    public SerialAutoIndexRepository(File directory)
    {
        SafeReturn<LocalSerialReader<Object, IndexedPersistableT, IndexT>>
                possibleReader = LocalSerialReader.make(directory);
        if(!possibleReader.getExceptions().isEmpty())
        {
            for(ExceptionInformation exInfo : possibleReader.getExceptions())
            {
                if(exInfo.getType().equals(IOExceptions.FILE_NOT_FOUND))
                {
                    //back to user and retry.
                }
                else
                {
                    //ran out of handles, ABORT!
                    System.out.println(exInfo.toString());
                    System.exit(1);
                }
            }
        }
        this.targetDirectory = directory;
        //this.cache = new ArrayList<>();
        //refreshCache();
    }
    /**
     * {@inheritDoc }
     */
    @Override
    public SafeReturn<CreationResult<Object, PrePersistableT, IndexedPersistableT, IndexT>> create(Collection<PrePersistableT> newObjects)
    {
        //Integer
        SafeReturn<CreationResult<Object, PrePersistableT, IndexedPersistableT, IndexT>> returnV = null;
        Collection<PrePersistableT> notCreated = new ArrayList<>(newObjects);//might become linked
        Collection<IndexedPersistableT> created = new ArrayList<>(newObjects.size()/2);
        Collection<ExceptionInformation> exceptions = new LinkedList<>();
        if(this.targetDirectory.isDirectory())
        {
            FileOutputStream out = null;
            try 
            {
                for(PrePersistableT singleP : newObjects)
                {
                    out = new FileOutputStream(new File(
                            this.targetDirectory.getPath() + 
                                    File.separatorChar));//TODO make string path!
                    ObjectOutputStream objectOutput = new ObjectOutputStream(out);
                    objectOutput.writeObject(singleP);
                    notCreated.remove(singleP);
                    objectOutput.close();
                    out.close();
                }
                
                
            } catch (Throwable ex) {
                ExceptionInformationBuilder builder = 
                        ExceptionInformationBuilder.newBuilder();
                builder.setMessage(builder.makeMessage(
                        Thread.currentThread().getName(),
                        ex.getMessage(), this.getClass().getName() + 
                                " create(Collection<N> newObjects)"));
                builder.setLevel(Level.SEVERE);
                builder.setThrowable(ex);
                builder.setThrower(this);
                exceptions.add(builder.build());
            }
        }
        return new SafeReturnConstructor().construct(
                new CreationResult(created, notCreated),exceptions);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public SafeReturn<Collection<IndexedPersistableT>> read()
    {
        return this.reader.read();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public SafeReturn<Collection<IndexedPersistableT>> update(Collection<IndexedPersistableT> newVersions)
    {
        for(IndexedPersistableT p : newVersions)
        {
            //if index is same as currently inspecting entry... update entry.
        }
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SafeReturn<Collection<IndexedPersistableT>> delete(Collection<IndexedPersistableT> removedObjects)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SafeReturn<Collection<IndexedPersistableT>> atomicRead()
    {
        return this.reader.atomicRead();
    }

    @Override
    public Collection<ExceptionInformation> atomicUpdate(Collection<IndexedPersistableT> newVersions)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<ExceptionInformation> atomicDelete(Collection<IndexedPersistableT> removedObjects)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SafeReturn<Boolean> clear()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SafeReturn<Collection<IndexedPersistableT>> atomicCreate(Collection<PrePersistableT> newTs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
