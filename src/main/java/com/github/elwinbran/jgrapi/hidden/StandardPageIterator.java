/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.jgrapi.hidden;

import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jerl.SafeReturnConstructor;
import com.github.elwinbran.jgrapi.DataOnlyPersistable;
import com.github.elwinbran.uei.ExceptionInformation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Elwin Slokker
 */
public class StandardPageIterator<PersistableT extends DataOnlyPersistable> 
        implements Iterator<SafeReturn<Collection<PersistableT>>>
{

    private final List<SafeReturn<Collection<PersistableT>>> pages;
    
    private int currentPage = 0;
    
    public StandardPageIterator(SafeReturn<? extends Iterable<PersistableT>> 
            originalSet, final int maxResultPerPage)
    {
        if(maxResultPerPage < 1)
        {
            throw new IllegalArgumentException("Pages cannot contain less than one item.");
        }
        if(originalSet.hasReturnValue())
        {
            this.pages = generatePages(originalSet.getReturn(), maxResultPerPage,
                    originalSet.getExceptions());
        }
        else
        {
            this.pages = Collections.EMPTY_LIST;
        }
    }
    
    public final List<SafeReturn<Collection<PersistableT>>> generatePages(
            Iterable<PersistableT> originalSet, final int maxResultPerPage,
            Collection<ExceptionInformation> exceptions)
    {
        List<SafeReturn<Collection<PersistableT>>> pagedList = new ArrayList<>();
        int counter = 0;
        Collection<PersistableT> items = new ArrayList<>(maxResultPerPage);
        for(PersistableT persist : originalSet)
        {
            items.add(persist);
            counter++;
            if(counter == maxResultPerPage)
            {
                //pagedList.add(SafeReturnConstructor[something]);
                counter = 0;
                items = new ArrayList<>(maxResultPerPage);
            }
        }
        return pagedList;
    }
    
    @Override
    public boolean hasNext()
    {
        return (this.currentPage != this.pages.size());
    }

    @Override
    public SafeReturn<Collection<PersistableT>> next()
    {
        if(this.currentPage == this.pages.size())
        {
            throw new NullPointerException("No more pages left to iterate.");
        }
        this.currentPage++;
        return this.pages.get(this.currentPage - 1);
    } 
}
