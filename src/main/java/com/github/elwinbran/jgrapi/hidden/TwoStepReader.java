/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.jgrapi.hidden;

import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jerl.SafeReturnConstructor;
import com.github.elwinbran.jgrapi.DataReader;
import com.github.elwinbran.jgrapi.GenericInterpreter;
import com.github.elwinbran.jgrapi.Reader;
import com.github.elwinbran.uei.ExceptionInformation;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Some reader that uses an interpreter. Two steps.
 * WIP until index problem is solved.
 * 
 * 
 * @author Elwin Slokker
 * 
 * 
 * @param <DomainT> Domain object type that {@code PersistableT} represents.
 * @param <PersistableT> Type of object that needs to be read/retrieved.
 * @param <IndexT> The index type {@code PersistableT} uses to be unique.
 * @param <DataT> Data type of the storage.
 * <br>Preferably {@code List<String>} or {@code byte[]}.
 */
public abstract class TwoStepReader<DomainT extends Object,
        PersistableT extends Persistable<DomainT, IndexT>,
        IndexT extends Serializable & Comparable<IndexT>,
        DataT extends Object>
        implements Reader<DomainT, PersistableT, IndexT> 
{
    /**
    * actually unsure if a reader can be
    * created with id's. Because, how would we know the id's?
    * To a directory you issue a read for all files (or when it is a compressed
    * file you always read and convert that.)
    * And database have a read query.
    * Maybe this is useful for HTTP remote storage 
    */
    private final ReaderCreator<DataT, IndexT> idReaderCreator;
    
   /**
    * The object that performs translation from and to the pure data.
    */
    private final GenericInterpreter<PersistableT, IndexT, DataT> interpreter;
    
    public TwoStepReader(ReaderCreator<DataT, IndexT> readerCreator, 
            GenericInterpreter<PersistableT, IndexT, DataT> interpreter)
    {
        this.idReaderCreator = readerCreator;
        this.interpreter = interpreter;
    }
    
//==============================================================================
//Implemented Methods
//==============================================================================
    /**
     * {@inheritDoc }
     */
    @Override
    public SafeReturn<Collection<PersistableT>> read()
    {
        Collection<ExceptionInformation> exceptions = new LinkedList<>();
        SafeReturn<Collection<IndexT>> possibleIndices = getIndices();
        exceptions.addAll(possibleIndices.getExceptions());
        if(possibleIndices.hasReturnValue())
        {
            return read(possibleIndices.getReturn(), exceptions);
        }
        return new SafeReturnConstructor().construct(exceptions);
    }

    /**
     * {@inheritDoc }
     */
    @Deprecated
    @Override
    public SafeReturn<Collection<PersistableT>> atomicRead()
    {
        Collection<ExceptionInformation> exceptions = new LinkedList<>();
        SafeReturn<Collection<IndexT>> possibleIndices = getIndices();
        exceptions.addAll(possibleIndices.getExceptions());
        if(possibleIndices.hasReturnValue())
        {
            return atomicRead(possibleIndices.getReturn().iterator(), exceptions);
        }
        return new SafeReturnConstructor().construct(exceptions);
    }
//==============================================================================
//Introduced methods
//==============================================================================
    
    /**
     * Returns all the indices of the entries that persist in the database.
     * 
     * @return 
     */
    public abstract SafeReturn<Collection<IndexT>> getIndices();
    
    /**
     * Follow-up method of {@link #atomicRead()}.
     * 
     * @param iterator
     * @param exceptions
     * @return 
     */
    public SafeReturn<Collection<PersistableT>> atomicRead(
            Iterator<IndexT> iterator, 
            Collection<ExceptionInformation> exceptions)
    {
        Collection<PersistableT> readObjects = new LinkedList<>();
        while(iterator.hasNext())
        {
            SafeReturn<PersistableT> possibleObject = 
                    read(idReaderCreator.getReader(iterator.next()));
            exceptions.addAll(possibleObject.getExceptions());
            if(possibleObject.hasReturnValue())
            {
                readObjects.add(possibleObject.getReturn());
            }
            else
            {
                return atomicReadFailed(iterator, exceptions);
            }
        }
        return new SafeReturnConstructor().construct(readObjects, exceptions);
    }
    
    /**
     * Used when a read operation failed.
     * This method will not actually return any read object.
     * It does however return all exceptions.
     * 
     * @param iterator
     * @param exceptions
     * @return 
     */
    public SafeReturn<Collection<PersistableT>> atomicReadFailed(
            Iterator<IndexT> iterator, 
            Collection<ExceptionInformation> exceptions)
    {
        while(iterator.hasNext())
        {
            SafeReturn<PersistableT> possibleObject = 
                    read(idReaderCreator.getReader(iterator.next()));
            exceptions.addAll(possibleObject.getExceptions());
        }
        return new SafeReturnConstructor().construct(exceptions);
    }
    
    /**
     * 
     * @param indices
     * @param exceptions
     * @return 
     */
    public SafeReturn<Collection<PersistableT>> read(Collection<IndexT> indices,
            Collection<ExceptionInformation> exceptions)
    {
        Collection<PersistableT> readObjects = new LinkedList<>();
        for(IndexT index : indices)
        {
            SafeReturn<PersistableT> possibleObject = 
                    read(idReaderCreator.getReader(index));
            exceptions.addAll(possibleObject.getExceptions());
            if(possibleObject.hasReturnValue())
            {
                readObjects.add(possibleObject.getReturn());
            }
        }
        if(readObjects.isEmpty())
        {
            return new SafeReturnConstructor().
                    construct(exceptions);
        }
        return new SafeReturnConstructor().construct(readObjects, exceptions);
    }
    
    /**
     * Reads data and translates.
     * Extracted from the other method to remove duplicate.
     * 
     * @param reader An object that returns pure data that makes up exactly
     * one entry in the repository/storage.
     * @return Possibly a successfully retrieved object.
     */
    public SafeReturn<PersistableT> read(DataReader<DataT> reader)
    {
        Collection<ExceptionInformation> exceptions = new LinkedList<>();
        SafeReturn<DataT> possibleData = reader.readSource();
        exceptions.addAll(possibleData.getExceptions());
        if(possibleData.hasReturnValue())
        {
            SafeReturn<PersistableT> possibleObject = 
                    interpreter.interpret(possibleData.getReturn());
            exceptions.addAll(possibleObject.getExceptions());
            if(possibleObject.hasReturnValue())
            {
                new SafeReturnConstructor<>().construct(
                        possibleObject.getReturn(), exceptions);
            }
        }
        return new SafeReturnConstructor<PersistableT>().construct(exceptions);
    }
}
