
package com.github.elwinbran.jgrapi.hidden;

import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jerl.SafeReturnConstructor;
import com.github.elwinbran.jgrapi.DataReader;
import com.github.elwinbran.jgrapi.GenericInterpreter;
import com.github.elwinbran.jgrapi.ReadOnlyIndexable;
import com.github.elwinbran.jgrapi.experimental.wrapped.WrappingFreeIndexRepository;
import com.github.elwinbran.uei.ExceptionInformation;
import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

/**
 * TODO, maybe really an internal class not to be used by the user in any way?
 * <br>Anyway, defines a repository that includes an interpreter, along with some 
 * support code.
 * 
 * 
 * @author Elwin Slokker
 * @param <T> The object type to be stored in the repository.
 * @param <I> The index type {@code T} uses to be unique.
 * @param <D> Internal datatype of repository. 
 * <br>Preferably {@code List<String>} or {@code byte[]}.
 * 
 * 
 * @deprecated That this class needs 3 parameters is not a good sign.
 */
@Deprecated
public abstract class TwoStepRepository<T extends Serializable & 
        ReadOnlyIndexable<I>, I extends Serializable & Comparable<I>, 
        D extends Object> 
        extends WrappingFreeIndexRepository<T, I>
{
    private final DataReader<D> reader;
    private final WriterCreator<D, I> writerCreator;
    private final GenericInterpreter<T, I, D> interpreter;
    private final TwoStepReader<T, I, D> wrappedReader;
    
    public TwoStepRepository(DataReader<D> reader, WriterCreator<D, I> writer,
            GenericInterpreter<T, I, D> interpreter, 
            TwoStepReader<T, I, D> wrappedReader)
    {
        this.reader = reader;
        this.writerCreator = writer;
        this.interpreter = interpreter;
        this.wrappedReader = wrappedReader;
    }
    
    /**
     * {@inheritDoc }
     */
    @Override
    public SafeReturn<Collection<T>> create(
            Collection<T> newTs)
    {
        Collection<T> missedObjects = new LinkedList<>();
        Collection<ExceptionInformation> exceptions = new LinkedList<>();
        for(T tObject : newTs)
        {
            //final ExceptionReturnableBuilder<ExceptionInformation, D>
            //    builder = ExceptionReturnableBuilder.newBuilder();
            SafeReturn<D> dataToWrite = this.interpreter.marshal(tObject);
            //check data
            if(dataToWrite.hasReturnValue())
            {
                SafeReturn<Boolean> writeInfo = this.writerCreator.getWriter(
                        tObject.getIndex()).writeData(dataToWrite.getReturn());
                exceptions.addAll(writeInfo.getExceptions());
                if(writeInfo.hasReturnValue())
                {
                    if(!writeInfo.getReturn())
                    {
                        missedObjects.add(tObject);
                    }
                }
                
                //this actually needs to be handled by the subclass.
                //Or at least a new writer is needed. One writer always writes
                //to a single entry or file. So it cannot understand create.
                //this.writer.writeData(dataToWrite.getValue());
            }
            exceptions.addAll(dataToWrite.getExceptions());
            
        }
        //error because library under construction
        return new SafeReturnConstructor<Collection<T>>().construct(missedObjects, exceptions);
    }
}
