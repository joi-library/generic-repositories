/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.jgrapi.hidden;

import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jerl.SafeReturnConstructor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import com.github.elwinbran.jgrapi.Filterable;
import com.github.elwinbran.jgrapi.IndexedPersistable;
import com.github.elwinbran.jgrapi.ReadFilter;

/**
 * A class meant for testing read-only repositories. 
 * IS NOT ACTUALLY PERSISTANT, ONLY USE FOR TESTING.
 * 
 * @author Elwin Slokker
 * @param <PersistableT>
 * @param <IndexT>
 * 
 * @deprecated May be deleted at any time due to it being unnecessary. No code
 * should depend on this class.
 */
@Deprecated
public class VirtualReader<
        PersistableT extends IndexedPersistable<IndexT>,
        IndexT extends Serializable & Comparable<IndexT>>
        implements Filterable<PersistableT, IndexT>
{

    private Collection<PersistableT> virtualRepo = new ArrayList<>();
    

    @Override
    public SafeReturn<Collection<PersistableT>> atomicRead() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SafeReturn<Collection<PersistableT>> read() {
        return new SafeReturnConstructor<Collection<PersistableT>>().
                construct(new ArrayList<>(this.virtualRepo));
    }

    @Override
    public SafeReturn<Collection<PersistableT>> atomicRead(ReadFilter<? extends PersistableT, ? extends IndexT> filter) {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }

    @Override
    public SafeReturn<Collection<PersistableT>> read(ReadFilter<? extends PersistableT, ? extends IndexT> filter) {
        throw new UnsupportedOperationException("This is a null-implementation.");
    }
    
}
