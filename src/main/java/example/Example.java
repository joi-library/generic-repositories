/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package example;

import com.github.elwinbran.jerl.SafeReturn;
import com.github.elwinbran.jgrapi.DataOnlyPersistable;
import com.github.elwinbran.jgrapi.IndexedPersistable;
import com.github.elwinbran.jgrapi.ReadQueryBuilder;
import com.github.elwinbran.jgrapi.Reader;
import com.github.elwinbran.jgrapi.ReaderQueryable;
import com.github.elwinbran.jgrapi.SimpleReader;
import com.github.elwinbran.jgrapi.experimental.ReaderFactory;
import com.github.elwinbran.jgrapi.experimental.WrappingProvidedCommand;
import com.github.elwinbran.jgrapi.experimental.meta.DataFormat;
import com.github.elwinbran.jgrapi.experimental.wrapped.TrueWrappingPersistable;
import com.github.elwinbran.jgrapi.experimental.wrapped.WrappingFreeIndexRepository;
import com.github.elwinbran.jgrapi.experimental.wrapped.WrappingPersistable;
import com.github.elwinbran.jgrapi.experimental.wrapped.WrappingProvidedIndexRepository;
import com.github.elwinbran.jgrapi.experimental.wrapped.WrappingSimpleReader;
import com.github.elwinbran.jgrapi.hidden.LocalSerialSRImpl;
import com.github.elwinbran.jgrapi.hidden.LocalSerialWSRImpl;
import com.github.elwinbran.jgrapi.experimental.wrapped.WrappingReaderQueryable;
import com.github.elwinbran.jgrapi.DomainIndexRepository;
import com.github.elwinbran.jgrapi.IndexProvidingRepository;

/**
 *
 * @author Elwin Slokker
 */
public class Example
{
    public static void main(String[] args)
    {
        /*
        //How the API should look:
        Repository<DomainT> repo = null;//some creation method which requires URL/URI
        //safeReturn stuff omitted
        Collection<DomainT> readFromRepo = repo.read();
        
        
        RepoThatProvidesIndices repo = null;
        RepoIndicesNotProvided storage = null;
        Collection<X> domainData = null;//domain
        //-Wrapped domain objects
        Collection<DataOnlyPerWrapper<X>> persistableData = -> forEach(convert domainData);
        CreationReturn<DataOnlyPerWrapper<X>> creationResult = repo.create(persistableData).getReturn();
        
        The big thing is preventing the user from using persistableData in an 
        update or delete query.
        */
        Reader<TrueWrappingPersistable<Car, Long> carStorage = null;
    }
//==============================================================================
// test all repo cases
//==============================================================================
    private static void readers()
    {
        SimpleReader<Car, Index> sReader = new LocalSerialSRImpl<>(new File(""));
        ReaderQueryable<Car, Index> qReader = null;
        ReadQueryBuilder builder = qReader.newReadQueryBuilder();
        builder.setIndexFilter(new Index(6));
        SafeReturn<Collection<Car>> sixCar = builder.build().read();
        //automatic casting... aparantly this is pretty doable.
        //a reader with wrapped object 'proof'.
        WrappingSimpleReader<Car, CarWrappingPersister, Index> wReader = 
                new LocalSerialWSRImpl(new File(""));
        wReader.read().getReturn().iterator().next().getDomainObject().drive();
        WrappingReaderQueryable<Car, CarWrappingPersister, Index> qwReader;
        
        //factory test
        sReader =  ReaderFactory.getInstance().makeSimpleReader(null);
        /*
        how can reader/repo implementations be added to the table factories use?
        singleton mapping...?
        
        */
    }
    
    private static void repos()
    {
        IndexProvidingRepository<IndexedPCar, Index> repo = null;
        DomainIndexRepository<CarData, IndexedPCar, Index> repoTwo = null;
        repoTwo.add(new CarData()).getReturn().getPersistedObjects();
        WrappingFreeIndexRepository<Car, CarWrappingPersister, Index> repoThree;
        //repoThree.
        //TODO annoying 'more interfaces' problem.
        //WrappingProvidedIndexRepository<Car, CarData, CarWrappingPersister, Index>
                //repoFor = null;
        //repoFor.create(new CarData()).getReturn().getPersistedObjects();
    }
    
    private static final void repoUnwrappingAutoIndex()
    {
    }
    
    /*
    private static abstract class MultiReader<DomainT extends Object,
                        PersistableT extends Persistable<?, IndexT>,
                        IndexT extends Serializable & Comparable<IndexT>>
        implements SimpleReader<PersistableT, IndexT>, 
                   ReaderQueryable<PersistableT, IndexT>,
                   WrappingSimpleReader<DomainT, PersistableT, IndexT>
    {
        
    }
    */
    private static final class DomainCar
    {
        private String bellShit = "bell";
    }
    
    private static final class Car implements IndexedPersistable<Index>
    {
        DataOnlyPersistable carData = new CarData();
        private Index index;
        
        @Override
        public Index getIndex()
        {
            return index;
        }
        public void drive()
        {
            System.out.println("vroom");
        }
    }
    
    private static final class Index implements Serializable, Comparable<Index>
    {
        private Long index;
        
        Index(long index)
        {
            this.index = index;
        }
        
        @Override
        public int compareTo(Index o)
        {
            return this.index.compareTo(o.index);
        }
        
    }
    
    private static final class CarData implements DataOnlyPersistable<Object>
    {
        String data = "this is a test car";
    }
    
    private static final class IndexedPCar implements IndexedPersistable<Index>
    {
        private Index index;
        
        @Override
        public Index getIndex()
        {
            return index;
        }
        
    }
    
    private static final class CarWrappingPersister implements WrappingPersistable<Car, Index>
    {
        private final Car domainCar;
        private Index index;
        public CarWrappingPersister(Car domainCar)
        {
            this.domainCar = domainCar;
        }
        @Override
        public Car getDomainObject()
        {
            return this.domainCar;
        }

        @Override
        public Index getIndex()
        {
            return index;
        }
    }
    private static final class CarPersister implements DataOnlyPersistable
    {
        protected final Car domainCar;
        public CarPersister(Car domainCar)
        {
            this.domainCar = domainCar;
        }
    }
    
    
    private static Collection<CarPersister> makePersisters(Collection<Car> cars)
    {
        final Collection<CarPersister> persisters = new LinkedList<>();
        for(Car car : cars)
        {
            persisters.add(new CarPersister(car));
        }
        return persisters;
    }
}
