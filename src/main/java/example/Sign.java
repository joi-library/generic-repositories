/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package example;

import com.github.elwinbran.jgrapi.experimental.PrimitiveSerializable;
import com.github.elwinbran.jgrapi.redesign.IndexDefinition;
import com.github.elwinbran.jgrapi.redesign.PersistableReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javafx.util.Pair;

/**
 *
 * @author Elwin Slokker
 */
public class Sign implements IndexDefinition
{
    private final static String PROPERTY_NAME = "sign";
    
    /**
     * TODO
     */
    private final Iterable<Pair<String, PrimitiveSerializable>> composition;
    
    /**
     * TODO
     */
    private final String shortcutReference;
    
    /**
     * TODO
     * @param sign 
     */
    public Sign(final String sign)
    {
        Pair<String, PrimitiveSerializable> signProperty = 
                new Pair(PROPERTY_NAME, PrimitiveSerializable.make(sign));
        Collection<Pair<String, PrimitiveSerializable>> compo =
                new ArrayList<>(1);
        compo.add(signProperty);
        this.composition = compo;
        this.shortcutReference = sign;
    }
    
    @Override
    public boolean isSinglePrimitive()
    {
        return true;
    }

    @Override
    public Iterable<Pair<String, PrimitiveSerializable>> getPrimitives()
    {
        return this.composition;
    }

    @Override
    public Iterable<Pair<String, PersistableReference>> getComposedObjects()
    {
        return Collections.EMPTY_LIST;
    }

    @Override
    public int compareTo(IndexDefinition other)
    {
        if(other.isSinglePrimitive())
        {
            final Pair<?, PrimitiveSerializable> property = 
                    other.getPrimitives().iterator().next();
            if(property.getKey().equals(PROPERTY_NAME))
            {
                if(property.getValue().getValue() instanceof String)
                {
                    return this.shortcutReference.compareTo(
                            (String)property.getValue().getValue());
                }
            }
        }
        return IndexDefinition.binaryDataComparasion(this, other);
    }
}
